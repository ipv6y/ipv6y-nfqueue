# -*- coding: utf-8 -*-

import log
import subprocess

from dpkt import ip

_cmd = "ip6tables"


def protocol_str(protocol):
    if protocol == ip.IP_PROTO_TCP:
        return 'tcp'
    if protocol == ip.IP_PROTO_UDP:
        return 'udp'
    if protocol == ip.IP_PROTO_ICMP6:
        return 'icmpv6'
    return None


def format_address(addr, port=None):
    if port:
        return '[%s]:%s' % (format_address(addr), port)
    return addr.compressed


def flush(table=None):
    args = []
    if table:
        args.append('--table')
        args.append(table)
    args.append('--flush')
    cmd = [_cmd]
    cmd.extend(args)
    if not subprocess.call(cmd) is 0:
        return 'Failed to execute: %s' % cmd
    else:
        log.info('%s' % cmd)


def snat(
    table,
    protocol,
    source,
    sport,
    destination,
    dport,
    to_source,
    delete=False
):
    args = []
    if table:
        args.append('--table')
        args.append(table)
    if delete:
        args.append('--delete')
    else:
        args.append('--append')
    args.append('POSTROUTING')
    if protocol_str(protocol):
        args.append('--protocol')
        args.append(protocol_str(protocol))
    if source:
        args.append('--source')
        args.append(format_address(source))
    if sport:
        args.append('--sport')
        args.append(str(sport))
    if destination:
        args.append('--destination')
        args.append(format_address(destination))
    if dport:
        args.append('--dport')
        args.append(str(dport))
    args.append('--jump')
    args.append('SNAT')
    args.append('--to-source')
    args.append(format_address(to_source))
    cmd = [_cmd]
    cmd.extend(args)
    if not subprocess.call(cmd) is 0:
        return 'Failed to execute: %s' % cmd
    else:
        log.info('%s' % cmd)


def dnat(
    table,
    protocol,
    source,
    sport,
    destination,
    dport,
    to_destination,
    delete=False
):
    args = []
    if table:
        args.append('--table')
        args.append(table)
    if delete:
        args.append('--delete')
    else:
        args.append('--append')
    args.append('PREROUTING')
    if protocol_str(protocol):
        args.append('--protocol')
        args.append(protocol_str(protocol))
    if source:
        args.append('--source')
        args.append(format_address(source))
    if sport:
        args.append('--sport')
        args.append(str(sport))
    if destination:
        args.append('--destination')
        args.append(format_address(destination))
    if dport:
        args.append('--dport')
        args.append(str(dport))
    args.append('--jump')
    args.append('DNAT')
    args.append('--to-destination')
    args.append(format_address(to_destination))
    cmd = [_cmd]
    cmd.extend(args)
    if not subprocess.call(cmd) is 0:
        return 'Failed to execute: %s' % cmd
    else:
        log.info('%s' % cmd)
